// Code generated by protoc-gen-go. DO NOT EDIT.
// source: search.proto

/*
Package protorepo_search_go is a generated protocol buffer package.

It is generated from these files:
	search.proto

It has these top-level messages:
	SearchQuery
*/
package protorepo_search_go

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import service_asset "gitlab.com/amnix/protorepo-asset-go"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type SearchQuery struct {
	Term string `protobuf:"bytes,1,opt,name=term" json:"term,omitempty"`
}

func (m *SearchQuery) Reset()                    { *m = SearchQuery{} }
func (m *SearchQuery) String() string            { return proto.CompactTextString(m) }
func (*SearchQuery) ProtoMessage()               {}
func (*SearchQuery) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *SearchQuery) GetTerm() string {
	if m != nil {
		return m.Term
	}
	return ""
}

func init() {
	proto.RegisterType((*SearchQuery)(nil), "service_search.SearchQuery")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for Searcher service

type SearcherClient interface {
	SearchAsset(ctx context.Context, in *SearchQuery, opts ...grpc.CallOption) (*service_asset.AssetsMetaList, error)
}

type searcherClient struct {
	cc *grpc.ClientConn
}

func NewSearcherClient(cc *grpc.ClientConn) SearcherClient {
	return &searcherClient{cc}
}

func (c *searcherClient) SearchAsset(ctx context.Context, in *SearchQuery, opts ...grpc.CallOption) (*service_asset.AssetsMetaList, error) {
	out := new(service_asset.AssetsMetaList)
	err := grpc.Invoke(ctx, "/service_search.Searcher/SearchAsset", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for Searcher service

type SearcherServer interface {
	SearchAsset(context.Context, *SearchQuery) (*service_asset.AssetsMetaList, error)
}

func RegisterSearcherServer(s *grpc.Server, srv SearcherServer) {
	s.RegisterService(&_Searcher_serviceDesc, srv)
}

func _Searcher_SearchAsset_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchQuery)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SearcherServer).SearchAsset(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/service_search.Searcher/SearchAsset",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SearcherServer).SearchAsset(ctx, req.(*SearchQuery))
	}
	return interceptor(ctx, in, info, handler)
}

var _Searcher_serviceDesc = grpc.ServiceDesc{
	ServiceName: "service_search.Searcher",
	HandlerType: (*SearcherServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SearchAsset",
			Handler:    _Searcher_SearchAsset_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "search.proto",
}

func init() { proto.RegisterFile("search.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 171 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x29, 0x4e, 0x4d, 0x2c,
	0x4a, 0xce, 0xd0, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0xe2, 0x2b, 0x4e, 0x2d, 0x2a, 0xcb, 0x4c,
	0x4e, 0x8d, 0x87, 0x88, 0x4a, 0x09, 0x26, 0x16, 0x17, 0xa7, 0x96, 0xe8, 0x83, 0x49, 0x88, 0x12,
	0x25, 0x45, 0x2e, 0xee, 0x60, 0xb0, 0x64, 0x60, 0x69, 0x6a, 0x51, 0xa5, 0x90, 0x10, 0x17, 0x4b,
	0x49, 0x6a, 0x51, 0xae, 0x04, 0xa3, 0x02, 0xa3, 0x06, 0x67, 0x10, 0x98, 0x6d, 0x14, 0xce, 0xc5,
	0x01, 0x51, 0x92, 0x5a, 0x24, 0xe4, 0x0d, 0x53, 0xee, 0x08, 0x32, 0x43, 0x48, 0x5a, 0x0f, 0xd5,
	0x06, 0x3d, 0x24, 0xb3, 0xa4, 0x64, 0xe1, 0x92, 0x10, 0x0b, 0xc1, 0x5a, 0x8a, 0x7d, 0x53, 0x4b,
	0x12, 0x7d, 0x32, 0x8b, 0x4b, 0x94, 0x18, 0x9c, 0xd4, 0xa2, 0x54, 0xd2, 0x33, 0x4b, 0x72, 0x12,
	0x93, 0xf4, 0x92, 0xf3, 0x73, 0xf5, 0x13, 0x73, 0xf3, 0x32, 0x2b, 0xf4, 0xc1, 0xae, 0x2a, 0x4a,
	0x2d, 0xc8, 0xd7, 0x85, 0x98, 0xa8, 0x9b, 0x9e, 0x9f, 0xc4, 0x06, 0x16, 0x34, 0x06, 0x04, 0x00,
	0x00, 0xff, 0xff, 0x97, 0x00, 0x2c, 0x88, 0xdd, 0x00, 0x00, 0x00,
}
